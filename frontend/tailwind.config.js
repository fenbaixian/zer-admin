/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      backgroundColor:{
        'back-dark':'#000000',
        'front-dark':'#141414',
        'lighter-dark':'#1d1d1d'
      }
    },
  },
  plugins: [],
  corePlugins: {
    preflight: false,  // 添加这一行
  },
  darkMode:'class'
};
