import {request} from "@/api/request.js";

export function getCode() {
  return request.post("/sec/code")
}
