import {request} from "@/api/request.js";

export function loginApi(form) {
  return request.post("/sec/login", form)
}

export function logoutApi() {
  return request.post("/sec/logout")
}

export function registerApi(form) {
  return request.post("/sec/signup", form)
}
