import {request} from "@/api/request.js";


export function getChannelAPI() {
  return request.get("/channels")
}

export function createArticleAPI(data) {
  return request.post("/mp/articles?draft=false", data)
}

export function getArticleById(id) {
  return request.get(`/mp/articles/${id}`)
}

// 获取文章列表

export function getArticleListAPI(params) {
  return request.get('/mp/articles', params)
}

// 删除文章

export function delArticleAPI(id) {
  return request.del(`/mp/articles/${id}`)
}

// 更新文章表单

export function updateArticleAPI(data) {
  return request.put(`/mp/articles/${data.id}?draft=false`, data)
}
