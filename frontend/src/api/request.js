import {http} from "@/api/http.js";

const get = (url, params = {}) => {
  return http.get(url, {params})
    .then(e => {
      return e.data
    })
}

const post = (url, data = {}) => {
  return http.post(url, data)
    .then(e => {
      return e.data
    })
}

const put = (url, data = {}) => {
  return http.put(url, data)
    .then(e => {
      return e.data
    })
}

const del = (url, data = {}) => {
  return http.delete(url, data)
    .then(e => {
      return e.data
    })
}

export const request = {
  get,
  post,
  put,
  del
}
