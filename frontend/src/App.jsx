import React, {useState} from 'react';
import {ConfigProvider} from "antd";

import {theme} from "antd";

import zhCN from 'antd/locale/zh_CN';
import {observer} from "mobx-react-lite";
import {RouterProvider} from "react-router-dom";
import {router} from "@/routes/index.jsx";
import {defaultStore} from "@/store/default-store.js";
import {useStore} from "@/store/index.js";


const App = observer(() => {
  const {layoutSettings} = useStore().defaultStore
  const [locale, setLocal] = useState(zhCN);

  defaultStore.initStore()

  return (
    // eslint-disable-next-line no-undef
    <ConfigProvider locale={locale}
                    theme={{algorithm: layoutSettings.menuTheme === 'dark' ? theme.darkAlgorithm : theme.defaultAlgorithm}}>
      <RouterProvider router={router}/>
    </ConfigProvider>
  )
})

export default App;
