// 存储所有的store
import {defaultStore} from "@/store/default-store.js";
import {createContext, useContext} from "react";
import {authStore} from "@/store/auth-store.js";

const stores = {
  authStore: authStore,
  defaultStore: defaultStore,
}

// 把store装载到context上下文进行存储
const mainStore = createContext(stores)

// 定义useStore钩子，调用我们装载的store
const useStore = () => {
  return useContext(mainStore)
}

export {useStore}
