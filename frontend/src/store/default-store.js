import {makeAutoObservable} from "mobx";
import {LocalKeys} from "@/constants/storage-key.js";
import {LocalUtil} from "@/utils/local-storage.js";

class DefaultStore {
  basicSettings = {
    lang: "zh"
  }
  layoutSettings = {
    menuExpand: true,
    menuTheme: 'light',
    configViewOpen: false,
    configViewSettings: [
      {
        name: "测试属性1",
        status: false
      },
      {
        name: "测试属性2",
        status: true
      },
    ]
  }

  constructor() {
    makeAutoObservable(this)
  }

  changeDrawer() {
    this.layoutSettings.configViewOpen = !this.layoutSettings.configViewOpen
  }

  // 保存用户配置
  saveConfigView() {
    this.layoutSettings.configViewOpen = false
    LocalUtil.setJsonLocal(LocalKeys.globalConfig, this.layoutSettings.configViewSettings)
  }

  initStore() {
    // 全局基础设置持久化
    let local = LocalUtil.getJsonLocal(LocalKeys.globalConfig);
    if (local !== null) {
      this.layoutSettings.configViewSettings = local
    }

    // 主题持久化
    let theme = LocalUtil.getSingleLocal(LocalKeys.themeConfig)
    if (theme !== null) {
      theme === 'dark' ? document.documentElement.classList.add('dark') : null
      this.layoutSettings.menuTheme = theme
    }
  }
}

export const defaultStore = new DefaultStore()
