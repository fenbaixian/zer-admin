import {makeAutoObservable} from "mobx";

class AuthStore {
  // 用户当前是否登录（后端校验token是否过期）
  isLogin = true

  constructor() {
    makeAutoObservable(this)
  }
}

export const authStore = new AuthStore()
