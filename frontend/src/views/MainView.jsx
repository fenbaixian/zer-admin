import React from 'react';
import {Card, Col, Row} from "antd";
import {BasicChartComp, CommonChartComp} from "@/components/index.js";
import {ChartOptions} from "@/constants/echart-options.js";

const MainView = () => {

  return (
    <>
      {/*<Row className='p-2  w-full' gutter={16}>*/}
      {/*  {[...Array(3)].map((_, index) => (*/}
      {/*    <Col key={index} span={8}>*/}
      {/*      <Card>*/}
      {/*        {comp}*/}
      {/*      </Card>*/}
      {/*    </Col>*/}
      {/*  ))}*/}
      {/*</Row>*/}

      <Row className='p-2 w-full' gutter={16}>
        <Col span={8}>
          <Card>
            <CommonChartComp title={"前端开发"} options={ChartOptions.barOption}/>
          </Card>
        </Col>
        <Col span={16}>
          <Card>
            <CommonChartComp title={"测试饼状图"} options={ChartOptions.pieOption}/>
          </Card>
        </Col>
      </Row>
    </>
  );
};

export default MainView;
