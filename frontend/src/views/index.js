export {default as MainView} from './MainView.jsx'

// antd组件测试
export {default as ButtonView} from '@/views/antd-comps/ButtonView.jsx'
export {default as TableView} from '@/views/antd-comps/TableView.jsx'

// 基本CRUD
export {default as ArticleSetView} from './crud/ArticleSetView.jsx'
export {default as ArticleShowView} from './crud/ArticleShowView.jsx'
export {default as ArticlePublishView} from './crud/ArticlePublishView.jsx'

// 用户信息栏
export {default as GlobalConfigView} from './personal/GlobalConfigView.jsx'
export {default as UserAccountView} from './personal/UserAccountView.jsx'
export {default as UserNoticeView} from './personal/UserNoticeView.jsx'


