import React from 'react';
import {observer} from "mobx-react-lite";
import {useStore} from "@/store/index.js";
import {Drawer, Switch} from "antd";
import {action} from "mobx";
import {defaultStore} from "@/store/default-store.js";

const handlerSave = () => {
  defaultStore.saveConfigView()
}

const GlobalConfigView = observer(() => {
  const {layoutSettings} = useStore().defaultStore
  return (
    <Drawer title="全局设置" placement="right" onClose={handlerSave}
            open={layoutSettings.configViewOpen}>
      <div className='w-full h-full dark:text-gray-200'>
        {
          layoutSettings.configViewSettings.map((item, index) => {
            return (
              <div key={index} className='w-full flex flex-row justify-between items-center mb-4'>
                <div className='mr-4'>{item.name}</div>
                <Switch defaultChecked={item.status} onClick={action(() => item.status = !item.status)}/>
              </div>
            )
          })
        }
      </div>
    </Drawer>
  );
})

export default GlobalConfigView;
