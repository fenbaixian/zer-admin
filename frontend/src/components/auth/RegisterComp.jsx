import React, {useState} from 'react';
import {Button, Checkbox, Form, Input, message, Space} from "antd";
import {useRouter} from "@/hooks/index.js";
import {BarcodeOutlined, LockOutlined, MailOutlined, UserOutlined} from "@ant-design/icons";
import {CaptchaComp} from "@/components/index.js";
import {registerApi} from "@/api/auth/login.js";
import {AuthUtil} from "@/utils/auth.js";

const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

const RegisterComp = () => {
  const [formReg] = Form.useForm();
  const [code, setCode] = useState('')
  const [componentDisabled, setComponentDisabled] = useState(false)
  const router = useRouter()
  const onFinish = async (values) => {
    console.log(values)
    setComponentDisabled(true)
    if (code !== values.code) {
      message.error("验证码错误请重填！", 1.5)
    } else {
      await registerApi(values).then(e => {
        setComponentDisabled(false)
        console.log(e)
        AuthUtil.setToken(e.data)
        router.replace("/dashboard")
        message.success("注册成功！", 1.5)
      }).catch(err => {
        setComponentDisabled(false)
      })
    }
  };

  const handleCode = (data) => {
    console.log(data)
    setCode(data)
  }

  return (
    <div>
      <Form
        form={formReg}
        disabled={componentDisabled}
        name="normal_login"
        className="login-form"
        initialValues={{
          remember: true,
        }}
        labelCol={{
          span: 10,
        }}
        wrapperCol={{
          span: 32,
        }}
        onFinish={onFinish}
      >
        <Form.Item
          name="email"
          rules={[
            {
              required: true,
              message: '请输入注册邮箱',
            },
          ]}
        >
          <Input prefix={<MailOutlined/>} placeholder="QQ/163/gamil"/>
        </Form.Item>
        <Form.Item
          name="name"
          rules={[
            {
              required: true,
              message: '请输入正确的用户名！',
            },
          ]}
        >
          <Input prefix={<UserOutlined/>} placeholder="注册用户名"/>
        </Form.Item>

        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              min: 3,
              message: '请输入正确的密码',
            },
          ]}
        >
          <Input
            prefix={<LockOutlined/>}
            placeholder="输入密码"
          />
        </Form.Item>

        <Form.Item
          name="code"
          rules={[
            {
              required: true,
              min: 4,
              max: 4,
              message: '填入校验码',
            },
          ]}
        >
          <div className='flex flex-row justify-center items-center'>
            <Input
              prefix={<BarcodeOutlined/>}
              placeholder="校验码"
            />
            <CaptchaComp setCurrentCode={handleCode}/>
          </div>
        </Form.Item>

        <Form.Item className="mt-4">
          <Button type="primary" htmlType="submit" className="w-full">
            注册
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default RegisterComp;
