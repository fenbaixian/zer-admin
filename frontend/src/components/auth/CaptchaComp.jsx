import React, {Component, useEffect, useState} from 'react';
import {genCode} from "@/utils/common.js";

class CaptchaComp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      codeUrl: ''
    }
  }

  requestCode = () => {
    let code = genCode()
    this.setState({
      codeUrl: "https://dummyimage.com/160x60/641/fff&text=" + code
    })
    // eslint-disable-next-line react/prop-types
    this.props.setCurrentCode(code)
  }

  componentDidMount() {
    this.requestCode()
  }

  render() {
    return (
      <div>
        <img style={{width: '80px', height: '30px'}}
             src={this.state.codeUrl} alt="asd" onClick={this.requestCode}/>
      </div>
    );
  }
}

export default CaptchaComp;
