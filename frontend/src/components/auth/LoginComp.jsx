import React, {useState} from 'react';
import {Button, Checkbox, Form, Input, message, Space} from "antd";
import {useRouter} from "@/hooks/index.js";
import {LockOutlined, UserOutlined} from "@ant-design/icons";
import {loginApi} from "@/api/auth/login.js";
import {AuthUtil} from "@/utils/auth.js";

const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

const LoginComp = () => {
  const [formLogin] = Form.useForm();
  const [componentDisabled, setComponentDisabled] = useState(false)
  const router = useRouter()
  const onFinish = async (values) => {
    console.log(values)
    setComponentDisabled(true)
    await loginApi(values).then(e => {
      console.log(e)
      setComponentDisabled(false)
      message.success("成功登录！", 1.5)
      AuthUtil.setToken(e.data)
      router.replace("/dashboard")
    }).catch(err=>{
      setComponentDisabled(false)
      message.error("登陆失败请重试", 1.5)
    })
  };

  return (
    <div>
      <Form
        form={formLogin}
        disabled={componentDisabled}
        name="normal_login"
        className="login-form"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
      >
        <Form.Item
          name="email"
          rules={[
            {
              required: true,
              message: '请输入注册邮箱！',
            },
          ]}
        >
          <Input prefix={<UserOutlined className="site-form-item-icon"/>} placeholder="邮箱"/>
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              min: 3,
              message: '请输入正确的密码',
            },
          ]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon"/>}
            type="password"
            placeholder="输入密码"
          />
        </Form.Item>
        <Form.Item name="remember" valuePropName="checked">
          <Checkbox>保持登录状态一个月</Checkbox>
        </Form.Item>

        <Form.Item className="mt-4">
          <Button type="primary" htmlType="submit" className="w-full">
            登录
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default LoginComp;
