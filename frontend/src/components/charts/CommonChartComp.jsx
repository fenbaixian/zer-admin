import React, {useEffect, useRef} from 'react';
import PropTypes from "prop-types";
import * as echarts from "echarts";

const CommonChartComp = ({title, options}) => {
  const chartRef = useRef(null)
  useEffect(() => {
    const chartDom = chartRef.current
    const myChart = echarts.init(chartDom)
    options['title'] = {'text': title}
    options && myChart.setOption(options)
  }, [title, options])

  return <div ref={chartRef} className="w-full h-72"></div>
};

CommonChartComp.prototype = {
  title: PropTypes.string,
  options: PropTypes.object
}

export default CommonChartComp;
