import React from 'react';
import {useStore} from "@/store/index.js";
import {BulbFilled, BulbOutlined} from "@ant-design/icons";
import {action} from "mobx";
import {Button, Tooltip} from "antd";
import {observer} from "mobx-react-lite";
import {LocalUtil} from "@/utils/local-storage.js";
import {LocalKeys} from "@/constants/storage-key.js";

const DarkThemeComp = observer(() => {
  const {layoutSettings} = useStore().defaultStore

  const changeTheme = () => {
    if (layoutSettings.menuTheme === 'dark') {
      layoutSettings.menuTheme = "light"
      document.documentElement.classList.remove('dark')
      LocalUtil.setSingleLocal(LocalKeys.themeConfig,'light')
    } else {
      layoutSettings.menuTheme = 'dark'
      document.documentElement.classList.add('dark')
      LocalUtil.setSingleLocal(LocalKeys.themeConfig,'dark')
    }
  }

  const title = '切换主题';

  return (
    <>
      <Tooltip title={title}>
        <Button type='text' icon={layoutSettings.menuTheme === 'dark' ? <BulbOutlined/> : <BulbFilled/>}
                onClick={action(changeTheme)} className="mr-1"/>
      </Tooltip>
    </>
  );
})

export default DarkThemeComp;
