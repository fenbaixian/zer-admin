import {Button, Tooltip} from "antd";
import {
  BellOutlined,
} from "@ant-design/icons";
import {observer} from "mobx-react-lite";
import {useRouter} from "@/hooks/index.js";
import UserProfileComp from "@/components/header-bar/UserProfileComp.jsx";
import FullScreenComp from "@/components/header-bar/FullScreenComp.jsx";
import DarkThemeComp from "@/components/header-bar/DarkThemeComp.jsx";


const HeaderBarComp = observer(() => {
    const router = useRouter()

    return (
      <div className="flex flex-row justify-center items-center">
        {/*切换全屏模式*/}
        <FullScreenComp/>
        {/*用户通知*/}
        <Tooltip title="用户通知">
          <Button type='text' icon={<BellOutlined/>} className='mr-1' onClick={() => router.push("/dashboard/notice")}/>
        </Tooltip>
        {/*主题切换*/}
        <DarkThemeComp/>
        {/*用户信息设置*/}
        <UserProfileComp/>
      </div>
    );
  }
)

export default HeaderBarComp;
