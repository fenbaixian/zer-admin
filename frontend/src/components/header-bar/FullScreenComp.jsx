import React, {useEffect, useState} from 'react';
import screenfull from "screenfull";
import {Button, message, Tooltip} from "antd";
import Icon, {BellOutlined, FullscreenExitOutlined, FullscreenOutlined} from "@ant-design/icons";

const click = () => {
  if (!screenfull.isEnabled) {
    message.warning("很抱歉，你的浏览器不支持全屏", 1.5);
    return false;
  }
  screenfull.toggle();
};

const FullScreenComp = () => {
  const [isFullscreen, setIsFullscreen] = useState(false);

  const change = () => {
    console.log("fuck")
    setIsFullscreen(screenfull.isFullscreen);
  };

  useEffect(() => {
    screenfull.isEnabled && screenfull.on("change", change);
    return () => {
      screenfull.isEnabled && screenfull.off("change", change);
    };
  }, []);

  const title = isFullscreen ? "取消全屏" : "全屏";
  const type = isFullscreen ? <FullscreenExitOutlined/> : <FullscreenOutlined/>;

  return (
    <div className="mr-1">
      <Tooltip placement="bottom" title={title}>
        <Button type='text' icon={type} onClick={click}/>
      </Tooltip>
    </div>
  );
};

export default FullScreenComp;
