// headerbar组件
export {default as HeaderSetting} from './header-bar/HeaderBarComp.jsx'

// 注册组件
export {default as LoginComp} from './auth/LoginComp.jsx'
export {default as RegisterComp} from './auth/RegisterComp.jsx'
export {default as CaptchaComp} from './auth/CaptchaComp.jsx'

// 装饰功能组件
export {default as HelmetComp} from './decoration/HelmetComp.jsx'

// 主界面main图表展示
export {default as BasicChartComp} from '@/components/charts/BasicChartComp.jsx'
export {default as CommonChartComp} from '@/components/charts/CommonChartComp.jsx'
