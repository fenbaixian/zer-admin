import React, {useEffect, useState} from 'react';
import {Helmet, HelmetProvider} from "react-helmet-async";
import {usePathname} from "@/hooks/index.js";

// 动态更新dashboard标题
const HelmetComp = () => {
  const loc = usePathname()
  useEffect(() => {
  }, [loc])

  return (
    <HelmetProvider>
      <div>
        <Helmet>
          <title>Antd Dashboard {loc}</title>
        </Helmet>
      </div>
    </HelmetProvider>
  );
};

export default HelmetComp;
