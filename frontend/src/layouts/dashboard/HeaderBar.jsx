import {Button, Layout} from "antd";
import {MenuUnfoldOutlined, MenuFoldOutlined} from '@ant-design/icons';
import {action} from "mobx";
import {observer} from "mobx-react-lite";
import {useStore} from "@/store/index.js";
import {HeaderSetting} from "@/components/index.js";

import '@/assets/style/theme/dark.less'

const {Header} = Layout;

const HeaderBar = observer(() => {
  const {layoutSettings} = useStore().defaultStore

  return (
    <>
      <Header className="m-0 p-0 flex flex-row justify-between text-center items-center bg-gray-100 dark:bg-back-dark"
              style={{height: "50px", lineHeight: "50px"}}>
        <Button type="text" onClick={action(() => {
          layoutSettings.menuExpand = !layoutSettings.menuExpand
          console.log("菜单栏状态改变")
        })}>
          {
            layoutSettings.menuExpand ? <MenuFoldOutlined/> : <MenuUnfoldOutlined/>
          }
        </Button>
        <HeaderSetting/>
      </Header>
    </>
  );
});

export default HeaderBar;

