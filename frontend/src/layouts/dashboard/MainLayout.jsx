import {Layout} from "antd";
import HeaderBar from "@/layouts/dashboard/HeaderBar.jsx";
import SideBar from "@/layouts/dashboard/SideBar.jsx";
import ContentBox from "@/layouts/dashboard/ContentBox.jsx";
import {Helmet} from "react-helmet-async";
import {HelmetComp} from "@/components/index.js";

const MainLayout = () => {

  return (
    <>
      <HelmetComp/>
      <Layout className="h-screen bg-white flex flex-row justify-between">
        <SideBar/>
        <Layout
          style={{
            padding: '0 12px 12px',
          }}
        >
          <HeaderBar/>
          <ContentBox/>
        </Layout>
      </Layout>
    </>
  );
};

export default MainLayout;
