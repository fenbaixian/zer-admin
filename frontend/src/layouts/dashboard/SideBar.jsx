import React from 'react';
import {Menu} from "antd";
import {
  AppstoreOutlined,
  DatabaseOutlined,
  HomeOutlined,
  LaptopOutlined,
  NotificationOutlined,
  UserOutlined
} from "@ant-design/icons";
import {observer} from "mobx-react-lite";
import {useStore} from "@/store/index.js";
import {useRouter} from "@/hooks/index.js";
import Sider from "antd/lib/layout/Sider.js";

import logo from '@/assets/icons/logo/logo.svg'

function getItem(label, key, icon, children, theme) {
  return {
    key,
    label,
    icon,
    children,
    theme,
  };
}

const items2 = [UserOutlined, LaptopOutlined, NotificationOutlined].map((icon, index) => {
  const key = String(index + 1);
  return {
    key: `sub${key}`,
    icon: React.createElement(icon),
    label: `subnav ${key}`,
    children: new Array(4).fill(null).map((_, j) => {
      const subKey = index * 4 + j + 1;
      return {
        key: subKey,
        label: `option${subKey}`,
      };
    }),
  };
});

const menuList = [
  getItem("口罩识别", "antd", <AppstoreOutlined/>, [
    getItem('图像识别', 'pic'),
    getItem('视频识别', 'video'),
  ]),
  getItem("用户信息", 'crud', <DatabaseOutlined/>, [
    getItem('文章浏览', 'articleShow'),
    getItem('文章修改', 'articleSet'),
    getItem('文章发布', 'articlePublish'),
  ])
]

const SideBar = observer(() => {
  const {layoutSettings} = useStore().defaultStore
  const router = useRouter()

  const clickMenuItem = (e) => {
    router.push("/dashboard/" + e.key)
  }

  return (
    <>
      {/*<div className="h-screen overflow-y-auto overflow-hidden">*/}
      {/*  */}

      {/*</div>*/}

      <Sider
        collapsed={!layoutSettings.menuExpand}
        collapsedWidth={60}
        defaultCollapsed={false}
        theme={'light'}
      >
        {
          layoutSettings.menuExpand ? (
            <div className="w-full text-center cursor-pointer h-12"
                 onClick={() => router.push("/dashboard")}>
              <img src={logo} alt="" className="h-12 w-12"/>
            </div>
          ) : (
            <div className="w-full text-center cursor-pointer h-12">
              <img src={logo} alt="" className="h-12 w-12"/>
            </div>
          )
        }
        <Menu
          mode="inline"
          defaultSelectedKeys={['forms']}
          defaultOpenKeys={['antd']}
          inlineCollapsed={!layoutSettings.menuExpand}
          // theme={layoutSettings.menuTheme}
          style={{
            borderRight: 0,
            // width: layoutSettings.menuExpand ? '200px' : '50px'
          }}
          items={menuList}
          onClick={(e) => clickMenuItem(e)}
        />
      </Sider>
    </>
  );
});

export default SideBar;
