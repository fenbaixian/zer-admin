import React from 'react';
import {Outlet} from "react-router-dom";
import {Content} from "antd/es/layout/layout.js";
import {theme} from "antd";

const ContentBox = () => {
  const {
    token: {colorBgContainer, borderRadiusLG},
  } = theme.useToken();

  return (
    <>
      <Content
        style={{
          padding: 8,
          margin: '0',
          backgroundColor: colorBgContainer,
          borderRadius: borderRadiusLG,
        }}
      >
        {/*主路由界面*/}
        <div className='w-full h-full dark:text-gray-200 overflow-auto'>
          <Outlet/>
        </div>
      </Content>
    </>
  );
};

export default ContentBox;
