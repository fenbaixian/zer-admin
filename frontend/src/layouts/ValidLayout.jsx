import React from 'react';
import {useStore} from "@/store/index.js";
import {observer} from "mobx-react-lite";
import {Navigate} from "react-router-dom";

const ValidLayout = observer(({children}) => {
    const {authStore} = useStore()
    // if (authStore.) {
    //   return <>{children}</>
    // } else {
    //   return <Navigate to='/login' replace/>
    // }
    return (
      <>
        {authStore.isLogin ?
          <>{children}</> :
          <Navigate to='/login' replace/>
        }
      </>
    )
  }
)

export default ValidLayout
