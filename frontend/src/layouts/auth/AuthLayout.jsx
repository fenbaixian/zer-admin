import React from 'react';
import {Tabs} from "antd";
import {AuditOutlined, BookOutlined} from "@ant-design/icons";
import {LoginComp, RegisterComp} from "@/components/index.js";

const items = [
  {
    key: '1',
    label: '登录',
    children: <LoginComp/>,
  },
  {
    key: '2',
    label: '注册',
    children: <RegisterComp/>,
  }
];

const AuthLayout = () => {
  return (
    <div className="h-screen w-screen justify-center items-center flex bg-gray-200">
      <div className="w-96 h-72 flex flex-col items-center border-r-2 ">
        <Tabs
          defaultActiveKey="1"
          items={items}
        />
      </div>
    </div>
  );
};

export default AuthLayout;
