import React from 'react';
import {Empty, message} from "antd";


const NotFoundLayout = () => {
  message.error("页面请求错误404", 1)
  return (
    <div className="h-screen w-screen flex justify-center items-center">
      <Empty/>
    </div>
  );
};

export default NotFoundLayout;
