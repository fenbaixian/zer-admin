import {AuthLayout, NotFoundLayout} from "@/layouts/index.js";
import {Navigate} from "react-router-dom";

export const routerOther = [
  {
    path: "/login",
    element: <AuthLayout/>
  },
  {
    path: "/404",
    element: <NotFoundLayout/>
  },
  {
    path: "*",
    element: <Navigate to='/404' replace/>
  },
  {
    path: "/",
    element: <Navigate to="/dashboard" replace/>
  }
]
