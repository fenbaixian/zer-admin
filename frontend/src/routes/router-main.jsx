import {MainLayout, ValidLayout} from "@/layouts/index.js";
import {
  ArticlePublishView,
  ArticleSetView,
  ArticleShowView,
  MainView,
  TableView,
  UserAccountView,
  UserNoticeView
} from "@/views/index.js";

// const dashboardPath = [
//   "table",
//   "account",
//   "notice"
// ]

export const routerMain = {
  // ValidLayout实现简单的路由守卫功能
  path: "/dashboard",
  element: <ValidLayout><MainLayout/></ValidLayout>,
  children: [
    // 主看板
    {
      index: true,
      element: <MainView/>
    },

    // 用户信息设置
    {
      path: "/dashboard/account",
      element: <UserAccountView/>
    },
    {
      path: "/dashboard/notice",
      element: <UserNoticeView/>
    },

    // 图像识别测试
    {
      path: "/dashboard/pic",
      element: <TableView/>
    },
    {
      path: "/dashboard/video",
      element: <TableView/>
    },

    // 基础CRUD
    {
      path: "/dashboard/articleShow",
      element: <ArticleShowView/>
    },
    {
      path: "/dashboard/articleSet",
      element: <ArticleSetView/>
    },
    {
      path: "/dashboard/articlePublish",
      element: <ArticlePublishView/>
    },
  ]
}
