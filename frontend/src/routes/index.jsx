import {createBrowserRouter} from "react-router-dom";

import {routerOther} from "@/routes/router-other.jsx";
import {routerMain} from "@/routes/router-main.jsx";

const routes = [
  routerMain
];

routerOther.map(e => routes.push(e))

export const router = createBrowserRouter(routes);
