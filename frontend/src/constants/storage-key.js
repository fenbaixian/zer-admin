export const LocalKeys = {
  // 用户相关key
  userInfo: 'user-info',
  account: 'account-info',

  // 设置相关key
  globalConfig: 'global-config',
  // 全局主题
  themeConfig:'theme-config'
}

export const CookiesKeys = {
  token: "token-key",
}
