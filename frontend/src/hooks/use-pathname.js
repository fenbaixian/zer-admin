import {useLocation} from "react-router-dom";
import {useMemo} from "react";

export const usePathname = () => {
  const {pathname} = useLocation();
  return useMemo(() => pathname, [pathname]);
}
