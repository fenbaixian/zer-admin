import Mock from "mockjs/dist/mock.js";
import echarts from "@/mocks/echarts.js";

Mock.mock(/\/sec\/test/, 'get', echarts.forTest())

export default Mock
