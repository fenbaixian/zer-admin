export function genCode() {
  const characters = '0123456789abcdefghijklmnopqrstuvwxyz';
  let code = '';

  for (let i = 0; i < 4; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    code += characters.charAt(randomIndex);
  }

  return code;
}
