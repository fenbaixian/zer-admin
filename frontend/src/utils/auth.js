import Cookies from 'js-cookie'
import {CookiesKeys} from "@/constants/storage-key.js";

const TokenKey = CookiesKeys.token

function getToken() {
  return Cookies.get(TokenKey)
}

function setToken(token) {
  return Cookies.set(TokenKey, token)
}

function removeToken() {
  return Cookies.remove(TokenKey)
}

export const AuthUtil = {
  getToken,
  setToken,
  removeToken
}
