const setSingleLocal = (key, data) => {
  return localStorage.setItem(key, data)
}
const setJsonLocal = (key, data) => {
  return localStorage.setItem(key, JSON.stringify(data))
}

const getSingleLocal = (key) => {
  return localStorage.getItem(key)
}

const getJsonLocal = (key) => {
  return JSON.parse(localStorage.getItem(key))
}

const clearLocal = (key) => {
  return localStorage.removeItem(key)
}

export const LocalUtil = {
  setJsonLocal,
  setSingleLocal,
  getSingleLocal,
  getJsonLocal,
  clearLocal
}

